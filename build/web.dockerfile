FROM nginx:1.10-alpine

ADD build/vhost.conf /etc/nginx/conf.d/default.conf

COPY public /var/www/public